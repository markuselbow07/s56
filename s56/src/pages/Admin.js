import { useState, useEffect, useContext } from 'react';
import { Form, Button, ListGroup } from 'react-bootstrap';
import { Redirect } from 'react-router-dom';
import { Link } from 'react-router-dom';
import UserContext from '../UserContext';
import Swal from 'sweetalert2';

// 

export default function Admin(){

	return (
		
			<Form className="mt-3 pt-5 mt-5">
		      
			  <Form.Group>
			  	
			  </Form.Group>

			  <Form.Group className="mt-5 pt-5 d-flex justify-content-around">
			  	<ListGroup defaultActiveKey="#link1">
			  		<Form.Text className="text-center mb-4">
			  			<h1>Products</h1>
			  		</Form.Text>
			  	    <ListGroup.Item className="rounded mb-3" action href="#link1">
			  	      View all products
			  	    </ListGroup.Item>
			  	    <ListGroup.Item className="rounded mb-3" action href="#link2">
			  	      View one product
			  	    </ListGroup.Item>
			  	    <ListGroup.Item className="rounded mb-3" action href="#link3">
			  	      Create a Product
			  	    </ListGroup.Item>
			  	    <ListGroup.Item className="rounded mb-3" action href="#link4">
			  	      Update a product
			  	    </ListGroup.Item>
			  	</ListGroup>
			  	
			  	<ListGroup defaultActiveKey="#link2">
			  		<Form.Text className="text-center mb-4">
			  			<h1>Users</h1>
			  		</Form.Text>
			  	    <ListGroup.Item className="rounded mb-3" action href="#link5">
			  	      View all users
			  	    </ListGroup.Item>
			  	    <ListGroup.Item className="rounded mb-3" action href="#link6">
			  	      View one user
			  	    </ListGroup.Item>
			  	    <ListGroup.Item className="rounded mb-3" action href="#link7">
			  	      Set user to admin
			  	    </ListGroup.Item>
			  	</ListGroup>

			  	<ListGroup defaultActiveKey="#link3">
			  		<Form.Text className="text-center mb-4">
			  			<h1>Orders</h1>
			  		</Form.Text>
			  	    <ListGroup.Item className="rounded mb-3" action href="#link8">
			  	      View all orders
			  	    </ListGroup.Item>
			  	    
			  	</ListGroup>
			  </Form.Group>

			</Form>

	)
}